#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
    int h;
    cout << ("Введите высоту пирамиды: ");
    cin >> h;
    for(int i = 1; i <= h; i++ )
    {
        cout << setw(h+1 - i) << " ";
        for(int q = 1; q <= i ; ++q)
        {
            cout << "#";
        }
        cout << " ";
        for(int j = 1; j <= i; ++j)
        {
            cout << "#";
        }
        cout << endl;
    }
    return 0;
}

